<!DOCTYPE html>
<html>
<title>RCGU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style.css">
<link rel="stylesheet" href="/style1.css">
<script src="/js/classie.js"></script>
<script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 100,
                header = document.querySelector("header");
				navigation = document.querySelector("navigation");
				var d = document.getElementById("navigation");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
				d.className = "navigation smaller";

            } else {
                if (classie.has(header,"smaller")||classie.has(navigation,"smaller")) {
                    classie.remove(header,"smaller");
					d.className = "navigation";
                }
            }
        });
    }
    window.onload = init();
</script>
<script>
 $(document).ready(function() {
    $(window).resize(function(){
            var width = $(window).width();

            if(width > 700 ){
                $('#containerdiv').addClass('container clearfix');
				$('#no1').show();
				document.getElementById("home").style.width = "12%";
				document.getElementById("about").style.width = "12%";
				document.getElementById("bod").style.width = "12%";
				document.getElementById("members").style.width = "12%";
				document.getElementById("contact").style.width = "12%";
				document.getElementById("alumni").style.width = "12%";
				document.getElementById("register").style.width = "12%";
				$('#no2').show();
            }
            else if(width <= 700){
                $('#containerdiv').removeClass('container clearfix');
				$('#no1').hide();
				document.getElementById("home").style.width = "14%";
				document.getElementById("about").style.width = "14%";
				document.getElementById("bod").style.width = "14%";
				document.getElementById("members").style.width = "14%";
				document.getElementById("contact").style.width = "14%";
				document.getElementById("alumni").style.width = "14%";
				document.getElementById("register").style.width = "14%";
				$('#no2').hide();
            }


            })

.resize();

    });
</script>

<div id="headerr">
<header>
<div class="container clearfix" id="containerdiv">
<div class="w3-row" >
  <div class="w3-col s4 w3-center">
   <img src="/images/rotaract_logo.png" id="logo" style="float: left;">
  </div>
  <div class="w3-col s5  w3-center" >
   <img src="/images/rcgu_logo.png" id="logo" style="margin: 0 auto;">
  </div>
  <div class="w3-col s3  w3-center">
 <img src="/images/rotary2016_logo.png" id="logo" style="float: right;">
  </div>

</div>
</div>
</header>
<div>

</div>
<div id="navigation" class="navigation">
<ul class="w3-navbar w3-card-8  w3-white w3-center">
  <ul style="width:8%" id="no1">&nbsp;</ul>
  <ul id="home"><a href="/">Home</a></ul>
  <ul id="about"><a href="/about/">About</a></ul>
  <ul id="bod"><a href="/bod/">BOD</a></ul>
  <ul id="members" class="dropdown" class="dropbtn"><a href="/certificates/">Members</a>
    <div class="dropdown-content">
        <a href="/certificates/">2015-16</a>
        <a href="/certificates/2014-15/">2014-15</a>
      </div>
  </ul>
  <style>
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    width: 12.2%;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}
.dropdown:hover .dropdown-content {
    display: block;
}
</style>
  <ul id="contact"><a href="/contact/">Contact</a></ul>
  <ul id="alumni" class="dropdown" class="dropbtn"><a href="javascript:void(0)">Alumni</a>
    <div class="dropdown-content">
        <a href="/alumni/2015-16/">2015-16</a>
        <a href="/alumni/2014-15/">2014-15</a>
        <a href="/alumni/2013-14/">2013-14</a>
        <a href="/alumni/2012-13/">2012-13</a>
        <a href="/alumni/2011-12/">2011-12</a>
        <a href="/alumni/2010-11/">2010-11</a>
        <a href="/alumni/2009-10/">2009-10</a>
      </div>
  </ul>
  <ul id="register"><a href="/register/">Register</a></ul>
  <ul style="width:8%" id="no2">&nbsp;</ul>
</ul>
</div>
<script type="text/javascript">
jQuery(function($) {
var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
//alert($('ul a').length);
$('ul a').each(function() {
    if ((this.href === path||(path.includes("alumni")&&this.href.includes("javascript")))) {
        $(this).addClass('w3-grey');
    }
    //alert(this.href+"  "+path);
});
});
</script>

<div id="spacerr1" class="spacer1" style="background-color:white;">
  </div>

  <div class="spacer" >
    &nbsp;
</div>

</div>

</html>
