<!DOCTYPE html>
<html lang="en">
<title>Alumni | RCGU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style.css">
<link rel="stylesheet" href="/style1.css">
<head>
<meta charset="utf-8">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/favicon/manifest.json">
<link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#343e00">
<link rel="shortcut icon" href="/images/favicon/favicon.ico">
<meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

</head>

<body >
<?php
include('headerfile.php');
?>
<br>
<div class="container" style="z-index:100">
<h1 style="text-align:center;"><strong>Alumni</strong></h1>

<?php
include_once 'dbconnect.php';
?>

<?php
$year=$_GET["year"];
if($year==""){
$query = "SELECT DISTINCT Year FROM alumni ORDER BY Year DESC";
$result = mysqli_query($con,$query);
while($row = mysqli_fetch_array($result)){
  echo "<h1 style=\"text-align:center;\"><strong>".$row['Year']."</strong></h1>";
  $query1 = "SELECT * FROM alumni WHERE Year='".$row['Year']."' ORDER BY Post ASC";
  $result1 = mysqli_query($con,$query1);
  echo "<table style=\"word-wrap:break-word\"><tr>";
  while($row1 = mysqli_fetch_array($result1)){
    echo "<td width=\"50%\">";
    echo "<div class=\"imagestyle\"><img src=\"/images/alumni/".$row1[id].".jpg\" alt=\"".$row1[Name]."\" align=\"center\"></div>";
    echo "<h3><strong>".$row1[Name]."</strong></h3>".$row1[Post];
    echo "</td>";
  }
  echo "</tr></table>";
}
mysqli_close($con);
}
else{
	echo "<h1 style=\"text-align:center;\"><strong>".$year."</strong></h1>";
  $query1 = "SELECT * FROM alumni WHERE Year='".$year."' ORDER BY Post ASC";
  $result1 = mysqli_query($con,$query1);
  echo "<table style=\"word-wrap:break-word\"><tr>";
  while($row1 = mysqli_fetch_array($result1)){
    echo "<td width=\"50%\">";
    echo "<div class=\"imagestyle\"><img src=\"/images/alumni/".$row1[id].".jpg\" alt=\"".$row1[Name]."\" align=\"center\"></div>";
    echo "<h3><strong>".$row1[Name]."</strong></h3>".$row1[Post];
    echo "</td>";
  }
  echo "</tr></table>";
}

?>

</div>

<br>
<?php
include('footerfile.php');
?>
</body>
</html>
