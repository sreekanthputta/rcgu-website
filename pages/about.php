<!DOCTYPE html>
<html>
<title>About Us | RCGU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style.css">
<link rel="stylesheet" href="/style1.css">
<head>
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/favicon/manifest.json">
<link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#343e00">
<link rel="shortcut icon" href="/images/favicon/favicon.ico">
<meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
</head>
<script type="text/javascript" src="/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/js/jssor.slider.mini.js"></script>
    <!-- use jssor.slider.debug.js instead for debug -->




<body>
<?php
include('headerfile.php');
?>
<br>

<h1 style="text-align:center;"><strong>About Us</strong></h1>

	<div class="container" style="z-index:100">    
    <style>
    .video-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px; height: 0; overflow: hidden;
    }

    .video-container iframe,
    .video-container object,
    .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    </style>

    <div class="w3-row" style="position:relative" >
      <div class="w3-col s6  w3-center">
        <div class="video-container" style="margin: 5%;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/lTtMmLPyZKs?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>

      </div>
      <div class="w3-col s6  w3-center">
        <div class="video-container" style="margin: 5%;">
    	<iframe width="560" height="315" src="https://www.youtube.com/embed/gXiDwjF0WOE?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>
      </div>
    </div>
<h1>Rotary </h1>
<p>Rotary International is a global community of committed professionals working together to serve others and advance peace. It is an international service organization whose stated human rights purpose is to bring together business and professional leaders in order to provide humanitarian services, encourage high ethical standards in all vocations, and to advance goodwill around the world. More than 1.2 million members in over 34000 Rotary Clubs worldwide volunteer in communities at home and aboard. </p>




<h1>Rotaract </h1>
<p>Rotaract is a service club for young men and women aged 18 to 30 who are dedicated to community and international services. Rotaract originally began as a Rotary International youth program in 1968 at Charlotte North Rotary Club in Charlotte, North Carolina, USA, and has grown into a major Rotary-sponsored organization of over 9,539 clubs spread around the world and 219,397 members. It is a service, leadership and community service organization for young men and women between the ages 18 to 30. Rotaract focuses on the development of young adults as leaders in their communities and workplaces. Clubs around the world also take part in international service projects, in a global effort to bring peace and international understanding to the world.<br>
Rotaract Clubs are self-governing and self-supporting and can either be university or community based. Individual Rotary Clubs sponsor Rotaract Clubs and offer guidance and support making the Rotaract Clubs true <b>&#8243;Partners in Service&#8243;</b> and key members of the family of Rotary. </p>




<h1>Rotaract Club of GITAM University</h1>
<p>Established in 2007, Rotaract Club of GITAM University is a university based club committed to serve the community through volunteer service projects and social outreach. RCGU is one of the 77 clubs in R.I. Dist. 3020,which comprises of six revenue districts viz. Krishna, West Godavari, East Godavari, Visakhapatnam, Vizianagaram & Srikakulam. The Rotaract Club of GITAM University works under the guidance of Rotary Club of Vishakhapatnam. RCGU consists of members who are committed to serve the society to enhance the local community through service projects.<b>&#8243;Fellowship through service&#8243;</b>is highly practiced in RCGU with members making new friends and exploring the world. The main objectives of RCGU are promotion of community service, development of fellowship, professional and personal development of its members, international understanding, to advance peace and goodwill around the world. RCGU has conducted and has been involved in many successful events and projects. RCGU conducts meeting&#8242;s regularly to discuss events and invites ideas from members of the club which would be implemented.Through service to self and the community, one can enjoy the benefits of professional networking and the advancement of peace, goodwill, and international understanding in the ever&#8722;changing 21st century.
Join RCGU to make good friends as well as worthwhile business contacts. Youll learn valuable new skills and grow as a person and you&#8242;ll be sure of a warm welcome from other Rotarians around the world whenever you&#8242;re on your travels or where ever you stay.  </p>



</div>

<br>
<?php
include('footerfile.php');
?>
</body>
</html>
