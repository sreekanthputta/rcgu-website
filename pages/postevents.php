<?php
session_start();
include_once 'dbconnect.php';
if(!isset($_SESSION['user']))
{
	header("Location: login?next=postevents");
}
else{
$res=mysqli_query($con, "SELECT * FROM registrations WHERE id=".$_SESSION['user']);
$userRow=mysqli_fetch_array($res);
if(!($userRow['Login Right']=='Admin')){
	header("Location: login");
}
}
?>
<!DOCTYPE html>
<html lang="en">

<title>Post an Event | RCGU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style.css">
<link rel="stylesheet" href="/style1.css">
<head>
<meta charset="utf-8">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/favicon/manifest.json">
<link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#343e00">
<link rel="shortcut icon" href="/images/favicon/favicon.ico">
<meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
		<script>
	$( function() {
		$( ".date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1900:' + new Date().getFullYear(),
			dateFormat: 'dd-mm-yy'
		});
	} );
	function setsamedate(){
		var date1 = document.getElementById('date1');
		var date2 = document.getElementById('date2');

		var Date1=date1.value;
		Date1=Date1.split("-");
		var newDate1=Date1[1]+"/"+Date1[0]+"/"+Date1[2];
		var Date2=date2.value;
		Date2=Date2.split("-");
		var newDate2=Date2[1]+"/"+Date2[0]+"/"+Date2[2];
		if((new Date(newDate1).getTime()>new Date(newDate2).getTime())||(!(new Date(newDate1).getTime()>new Date(newDate2).getTime())&!(new Date(newDate1).getTime()<new Date(newDate2).getTime()))){
			date2.value=date1.value;
		}
	}
	</script>
</head>
<?php


if(isset($_POST['btn-signup']))
{

	$name = mysqli_real_escape_string($con, $_POST['name']);
	$time1 = mysqli_real_escape_string($con, $_POST['time1']);
	$time2 = mysqli_real_escape_string($con, $_POST['time2']);
	$date1 = mysqli_real_escape_string($con, $_POST['date1']);
	$date2 = mysqli_real_escape_string($con, $_POST['date2']);
	$venue = mysqli_real_escape_string($con, $_POST['venue']);
	$desc = mysqli_real_escape_string($con, $_POST['desc']);
	$lead = mysqli_real_escape_string($con, $_POST['lead']);


	$name = trim($name);
	$time1 = trim($time1);
	$time2 = trim($time2);
	$date1 = trim($date1);
	$date2 = trim($date2);
	$venue = trim($venue);
	$desc = trim($desc);
	$lead = trim($lead);


		if(mysqli_query($con, "INSERT INTO events(`id`, `Name`, `Start Time`, `End Time`, `Start Date`, `End Date`, `Venue`, `Description`, `Photos`, `Lead`, `Posted by`) VALUES(NULL, '$name','$time1','$time2','$date1','$date2','$venue','$desc','0','$lead','$userRow[Pin]')"))
		{
		?>
			<script>alert('Event posted successfully');</script>
			<?php
		}
		else
		{
			?>
			<script>alert('Something went wrong, please try after some time or contact admin.');</script>
			<?php
		}
}

?>
<body>
<?php
include('headerfile.php');
?>
<p style="float:right; padding-right:20px; padding-top:20px"><?php echo $userRow['Name']." ".$userRow['Last Name'].", "?><a href="../logout">logout</a></p>
<br>
<div class="container" style="z-index:100">
<h1 style="text-align:center;"><strong>Post an Event</strong></h1>
<div id="errormsgdiv" style="float: right; width:75%; margin: 0 15px 15px 0"><h3 id="errormsg" style="color:white"></h3></div>
<p style="float: right; width:75%; margin: 0 15px 15px 0; color:red">*Required</p>
<form name="RCGU" method="post">
<div style=" width:75%;" class="feedback1"><input type="text" name="name" placeholder="Name of the Event*" required style="width:100%"/></div>
<div style=" width:75%;" class="feedback1"><div class="left" style=" width:40%;"><input type="text" class="time" name="time1" placeholder="Start Time*" required style="width:100%"/></div>
											<div class="right" style=" width:40%;"><input type="text" class="time" name="time2" placeholder="End Time*" required style="width:100%"/></div><br></div>
<div style=" width:75%;" class="feedback1"><div class="left" style=" width:40%;"><input type="text" class="date" id="date1" name="date1" placeholder="Start Date*" onChange="setsamedate()" required style="width:100%"/></div>
											<div class="right" style=" width:40%;"><input type="text" class="date" id="date2" name="date2" placeholder="End Date*" onChange="setsamedate()" required style="width:100%"/></div><br></div>
<div style=" width:75%;" class="feedback1"><input type="text" name="venue" placeholder="Venue*" required style="width:100%"/></div>
<div style=" width:75%;" class="feedback1"><textarea rows="10" name="desc" placeholder="Description*" required style="width:100%"></textarea></div>
<div style=" width:75%;" class="feedback1"><input type="text" name="lead" placeholder="Lead by*" required style="width:100%"/></div>
<div style=" width:75%;" class="feedback1"><tr>
<td><button type="submit" name="btn-signup" class="button" id="buttonid" style="width:100%;margin: 0 auto">Submit</button></td>
</tr></div>
</form>
</div>
<style>
.button {
  display: inline-block;
  padding: 15px 25px;
  font-size: 24px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #4CAF50;
  border: none;
  border-radius: 15px;
  box-shadow: 0 3px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>
<br>
<?php
include('footerfile.php');
?>
</body>
</html>
